# coding=utf-8

from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import render_to_response
from django.template import RequestContext
from .models import *
from .cart import Cart


def contex_force_shop(request):
    cart = Cart(request)
    total_price = cart.get_cart(request).total_price()

    data = {
        'settings': settings,
        'base_template': 'base.html', 
        'path_products_count_in_page': request.GET.get('count', 12),
        'colors': Color.objects.filter(),
        'variations': TagCollection.objects.filter(),
        'brands': Brand.objects.filter(),
        'cart': cart,
        'total_price': total_price,
        'total_items': cart.count(),
        'categories': Category.get_roots(),
    }
    return data
