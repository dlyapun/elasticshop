from rest_framework import serializers
from force_shop.models import Product


class ProductSerializer(serializers.ModelSerializer):

    product_image = serializers.ReadOnlyField()
    get_absolute_url = serializers.ReadOnlyField()

    class Meta:
        model = Product
        fields = ['id', 'name', 'price', 'product_image', 'discount', 'new_product', 'get_absolute_url']
