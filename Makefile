MANAGE=django-admin.py

run:
	PYTHONPATH=`pwd` DJANGO_SETTINGS_MODULE=project.settings $(MANAGE) runserver

migrate:
	PYTHONPATH=`pwd` DJANGO_SETTINGS_MODULE=project.settings $(MANAGE) migrate

make:
	PYTHONPATH=`pwd` DJANGO_SETTINGS_MODULE=project.settings $(MANAGE) makemigrations

shell:
	PYTHONPATH=`pwd` DJANGO_SETTINGS_MODULE=project.settings $(MANAGE) shell

port:
	sudo fuser -k 8000/tcp

make_message:
	django-admin makemessages -l ru -a

compile_message:
	django-admin.py compilemessages

freeze:
	pip freeze > requirements.txt

pip:
	pip install -r requirements.txt

mail:
	python -m smtpd -n -c DebuggingServer localhost:1025

super:
	echo "from accounts.models import User; User.objects.create_superuser('root', 'root@root.com', 'root')" | python manage.py shell
